/*
    Poiché i tipi delle action sono stringhe e queste possono essere soggette a errori di battitura o duplicati
    è meglio creare delle costanti che contengano tali stringhe
 */

export const ADD_SOME_LITER = 'ADD_SOME_LITER';
export const REMOVE_SOME_LITER = 'REMOVE_SOME_LITER';

/*
    addSomeLiter è un'action creator
 */
export function addSomeLiter(liters) {
    /*
    Gli action creator restituiscono delle action che sono oggetti
     */
    return {
        type: ADD_SOME_LITER,
        litri: liters
    }
}

export function removeSomeLiter(liters) {
    /*
    Non creando una chiave, JS ne crea una con lo stesso nome del parametro (liters) alla quale assegna il valore del parametro stesso
     */
    return {
        type: REMOVE_SOME_LITER,
        liters
    }
}

export const faiRifornimento = () => { // Questo è un action creator. Serve per gestire le chiamate asincrone (es: http) i cui risultati devono modificare lo state
    /*
        getState è opzionale e restituisce lo state.
        Questa è una possibilità ma non bisogna abusarne.
        E' sempre meglio scrivere il codice in modo tale da inviare dal reducer tutti campi dello state ci cui ho bisgno qui.
     */
    return ( dispatch, getState ) => {
        let { litri, litriTotali} = getState();

/*        setTimeout(() => { // Al posto della setTimeout vanno inserite le chiamate REST
            dispatch(addSomeLiter(litriTotali - litri)); // Va chiamata una funziona sincrona perché solo le funzioni sincrone possono modificare lo state
        }, 2000);*/

        const id = setInterval(clock, 1000);
        function clock() {
            if (litri === litriTotali) {
                clearInterval(id);
            } else {
                dispatch(addSomeLiter(1));
                litri++;
            }
        }
    }
};
