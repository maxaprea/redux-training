import {
    ADD_SOME_LITER,
    REMOVE_SOME_LITER,
    addSomeLiter,
    removeSomeLiter
} from './actions';

const initialState = {
    litri: 0,
    litriTotali: 30
};

/*
    Il reducer prende in ingresso 2 parametri: il primo rappresenta lo stato iniziale dell'App, il secondo l'azione da eseguire
 */
const reducer = (state = initialState, action) => {

    switch (action.type) {
        case ADD_SOME_LITER: {
            let { litri } = state;
            litri += parseInt(action.litri); //Nell'action creator la chiave si chiama "litri"
            return {
                ...state,
                litri
            };
        }
        case REMOVE_SOME_LITER: {
            let { litri } = state;
            litri -= parseInt(action.liters); //Nell'action creator la chiave si chiama "litri"
            return {
                ...state,
                litri
            };
        }
        default :
            return state;
    }
};

export default reducer;
