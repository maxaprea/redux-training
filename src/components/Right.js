import React from 'react';
import { connect } from "react-redux";

const livello = (props) => {
    return props.litri * 100 / props.litriTotali + '%';
};

const right = (props) => {

    return (
        <>
            <h1>I litri presenti sono {props.litri}</h1>
            <svg viewBox="0 0 100 100">
                <defs>
                    <linearGradient id="gradient-opacity" x1="1" x2="1" y1="1" y2="0">
                        <stop offset="0%"  className="bar"></stop>
                        <stop offset={livello(props)} className="bar"></stop>
                        <stop offset={livello(props)} className="clear"></stop>
                    </linearGradient>
                </defs>
                <rect x="30" y="5" width="40" height="80" fill="url(#gradient-opacity)" stroke="gray" strokeWidth=".5" rx="1" ry="1" />
            </svg>
        </>
    );
};

const mapStateToProps = state => {
    return {
        litri: state.litri,
        litriTotali: state.litriTotali,
    }
};

export default connect(mapStateToProps)(right);
