import React, { Component } from 'react';
import { connect } from "react-redux";
import {
    addSomeLiter,
    removeSomeLiter,
    faiRifornimento
} from '../store/actions';

class Left extends Component{
    constructor(props) {
        super(props);

        this.state = {
            litri: 0
        };

        this.handleLitri = this.handleLitri.bind(this);
    }

    handleLitri(e) {
        const litri = e.target.value;

        this.setState({
            litri: litri
        });
    }

    addRemoveLitri(action) {
        const { litri } = this.state;

        action === 'a' ? this.props.aggiungi(litri) : this.props.rimuovi(litri)
    }

    render() {
        return (
            <>
                <div id="form">
                    <input type="number"
                           value={this.state.litri}
                           onChange={this.handleLitri}
                           max={this.props.litriTotali}
                           min="0"
                           placeholder="Inserisci il numero di litri"/>
                </div>
                <button className="primary"
                        disabled={this.state.litri === 0 || this.props.litri === this.props.litriTotali}
                        onClick={this.addRemoveLitri.bind(this, 'a')}>
                    Aggiungi {this.state.litri > 0 && this.props.litri < this.props.litriTotali && this.state.litri} litri
                </button>
                <button className="danger"
                        disabled={this.state.litri === 0 || this.props.litri === 0}
                        onClick={this.addRemoveLitri.bind(this, 'r')}>
                    Rimuovi {this.state.litri > 0 && this.props.litri > 0 && this.state.litri} litri
                </button>
                <button className="success"
                        disabled={this.props.litri === this.props.litriTotali}
                        onClick={this.props.pieno}>
                    Fai il pieno
                </button>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        litri: state.litri,
        litriTotali: state.litriTotali
    }
};

const mapDispatchToProps = dispatch => {
    return {
        aggiungi: litri => dispatch(addSomeLiter(litri)),
        rimuovi: litri => dispatch(removeSomeLiter(litri)),
        pieno: () => dispatch(faiRifornimento())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Left);
