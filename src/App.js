import React from 'react';
import Left from "./components/Left";
import Right from "./components/Right";
import './App.css';

function App(){
    return (
        <div className="App">
            <div id="left">
                <Left />
            </div>
            <div id="right">
                <Right />
            </div>
        </div>
    );
}

export default App;
