import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from "redux";

import thunk from 'redux-thunk';

/*
    Il reducer è una funzione che genera/modifica lo stato dell'applicazione.
    Lo stato in Redux è immutabile quindi lo stato prodotto dal reducer è una copia di quello originale
 */
import reducer from "./store/reducer";

/*
    Lo store contiene tutto lo stato dell'intera applicazione.
    Viene creato tramite createStore che vuole in ingresso il reducer
 */
const logger = store => {
    return next => {
        return action => {
            console.log('Middleware Dispatching', store.getState()); // Prima del dispatching
            const result = next(action);
            console.log('Middleware next state', store.getState()); // Dopo il dispatching
            return result;
        }
    }
};

const store = createStore(reducer, applyMiddleware(logger, thunk));

const app = (
    <Provider store={store}>
        <App />
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

serviceWorker.unregister();
