## Redux

Redux = state manager

### Come funziona Redux
-   Lo stato è <b>immutabile</b>
-   Lo state viene modificato tramite la funzione <b>reducer</b>
-   L'unico modo per modificare lo state è inviare un segnale al reducer. 
    Questo segnale si chiama <b>action</b>. Inviare un segnale si dice <b>dispatching</b>
-   Ogni action è un oggetto contenente il tipo di azione e il payload che rappresenta un oggetto che modifica lo state
-   Una <b>best practice</b> è creare una funzione che restituisca ogni action. 
    Tale funzione si chiama <b>action creator</b>
    
